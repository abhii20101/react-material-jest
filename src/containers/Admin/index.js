import React, { Component } from "react";
import AppRoutes from "./../../routes";
import environments from './../../environments'
import {Login} from './../../components';
import { BrowserRouter as Router, Link, Redirect, withRouter} from "react-router-dom";

 class Admin extends Component {
    constructor(){
        super()
        if(localStorage.getItem('token')){
            this.state={isLoggedIn: true};
        } else{
            this.state={isLoggedIn: false};
        }
    }

    handleLogin=(isLoggedIn)=>{
        this.setState({ ...this.state, isLoggedIn:isLoggedIn} )
    }

    logOut=()=>{
        localStorage.clear()
        this.props.history.push('/')
    }

    render() {
        debugger
        return (
            <div>
                {
                    this.state.isLoggedIn ? 
                    <div>isLoggedIn  {environments.API_URL} 
                    <li>
                                <span onClick={this.logOut}>Logout</span>
                            </li>:
                    </div>
                    
                    :
                     <Login login={this.handleLogin}/>
                }
                
            </div>
        );
    }
}

export default withRouter(Admin);