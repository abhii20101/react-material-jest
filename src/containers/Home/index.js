import React, { Component } from "react";
import {ProductList, Currency} from './../../components';
import { BrowserRouter as Router, Link, Redirect, withRouter} from "react-router-dom";
import {Request} from './../../util'


export default class Home extends Component {
    constructor(){
        super()
        this.state={}
        this.paid = 0;
    }

    payMentHadnler=(paid)=>{
        console.log("before",this.paid)
        this.paid= this.paid+ paid;
        console.log("after",this.paid)
        if(this.paid>=this.price){
            console.log("call");
            Request({
            method: 'post',
            url: '/apis/stateless/vending/v1.0/buy/product',
            data:   {
                name:this.product.name,
                value: this.paid
              }
          }).then((resp) => {
            console.log(resp);
            //debugger
            alert(`Bought ${this.product.name}`);
          }).catch(e=>{
              //debugger
              //this.props.login(false);
              console.log(e);
          })
        }
        
    }
    returnHandler=()=>{
        alert(`Returned ${this.paid} $`)
        this.paid= 0;
    }
    productSelectHandler=(product)=>{
        //debugger
        let selectedProduct= JSON.parse(product)
        this.price=selectedProduct.price;
        this.product=selectedProduct;
        //alert(this.price, JSON.stringify(this.product));
    }
    render() {
        return (
            <div>
              <li>
                  <Link to="/admin">Service</Link>
              </li>
              <ProductList productSelect={this.productSelectHandler}/>
              <Currency paymentDone={this.payMentHadnler}/>
                <br/>
              <button onClick={this.returnHandler}>Return</button>
            </div>
        );
    }
}
