const environments = {
    development: {
        API_URL:"http://52.151.79.76:8080"
        
    },
    production: {
        API_URL:''
    }
};

export default environments[process.env.REACT_APP_ENV] ||
    environments[Object.keys(environments)[0]];
