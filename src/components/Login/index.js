import React, { Component } from "react";
import environments from './../../environments'
import {Request} from './../../util';
import './index.css'
export default class Login extends Component {
    constructor(){
        super();
    }

    onClickHandler=()=>{
        //call api if token and 200 status else show error message
        Request({
            method: 'post',
            url: '/apis/stateless/vending/v1.0/login',
            data:   {
                username:document.getElementById('email').value,
                password: document.getElementById('password').value
              }
          }).then((resp) => {
            localStorage.setItem('token', resp.data);
            this.props.login(true);
            console.log(resp);
            debugger
          }).catch(e=>{
              debugger
              this.props.login(false);
              console.log(e);
          })

    }
    render() {
        return (
            <div>
                <div class="row">

                    <div class="col-xl">
                    <div class="card" >
                        
                        <div class="card-header"><h2>Login</h2></div>
                        <div class="card-body">
                        <form>
                            <input type="text" id="email" /><br/>
                            <input type="password" id="password" /><br/>
                            <button  type="button" class="btn btn-primary" onClick={this.onClickHandler} >Login</button>
                        </form>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}
