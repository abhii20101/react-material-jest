import React, { Component } from 'react'
import {Product} from './../../components';
import {Request} from './../../util'
export default class ProductList extends Component{
    constructor(){
        super()
        this.state={}
    }

    componentDidMount(){

        Request({
            method: 'get',
            url: '/apis/stateless/vending/v1.0/information'
          }).then((resp) => {
            console.log(resp);
            this.setState({...this.state, data:resp.data,error:null})
            debugger
          }).catch(e=>{
              debugger
              this.setState({...this.state, error:e})
              console.log(e);
          })
    }
    selectedProductHandler=(product)=>{
        this.props.productSelect(product.currentTarget.value);
    }

    render(){
        if(!this.state.data){
            return(<div>.....loading</div>)
        } else if(this.state.error){
            return(<div>Oops! Unable to load product</div>)
        }else{
        return(
                <form>
                <div class="row">
                    <div class="col-xl" >
                        <h3 >Items Available</h3></div>
                </div>
                {this.state.data.products.map(product => (
                <Product key={product.name} product={product} selectedProduct={this.selectedProductHandler}/>  
                 ))}
            </form>
            )
        }
    }
}