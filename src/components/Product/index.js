import React from 'react'
import image from './../../assets/images/packet.jpg';
const Product = (props) => {
    return(
        <div class="row">
            <div class="col-xl">
                <div class="card">

                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-2">
                                <img src={image} width="80" height="80" alt="packet" />
                            </div>
                            <div class="col-xl-5">
                                <h4>{props.product.name}<br/>${props.product.price}</h4>
                            </div>
                            <div class="col-xl-5">
                                <input type="radio" value={JSON.stringify(props.product)}  name="myProduct" onClick={props.selectedProduct}/>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Product;